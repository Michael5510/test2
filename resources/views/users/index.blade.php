@extends('layouts.app')

@section('title', 'Users')

@section('content')
@if(Session::has('notallowed'))
<div class = 'alert alert-danger'>
    {{Session::get('notallowed')}}
</div>
@endif
<h1>List of users</h1>
@csrf
<table class = "table table-dark">
    <tr>
        <th>id</th><th>Name</th><th>department</th><th>Created</th><th>Updated</th><th>role</th>
    </tr>
    <!-- the table data -->
    @foreach($users as $user)
        <tr>
            <td>{{$user->id}}</td>
            <td>{{$user->name}}</td>     
            @if(auth()->user()->isAdmin())
            <td>    
                {{$user->department->name}}
                

                <div>
                    <label for = "department_id">Assign department</label>
                    
                        @foreach($departments as $department)
                            <select class="form-control" name="user_id">
                                <option value="{{$department->id}}">{{$department->name}}</option>
                            </select>
                            <form action="{{ route('user.changedepartment', [$user->id, $department->id]) }}" method="GET">
                                <button type="submit" class="btn btn-success">
                                    <i class="fa fa-trash"></i> Change department
                                </button>
                            </form>
                        @endforeach
                    
                </div>
            </td>
            @else
                <td>{{$user->department->name}}</td>         
            @endif
            <td>{{$user->created_at}}</td>
            <td>{{$user->updated_at}}</td>
            <td>
            @foreach($user->roles as $role)
                <div>{{ $role->name }}</div>
            @endforeach
            </td>      

            <td>
                    <a href = "{{route('user.delete',$user->id)}}">Delete</a>
  
            </td> 

            <td>
                <a href = "{{route('users.show',$user->id)}}">Details</a>
            </td>     
            
            <td>
                @if($user->isManager())
                <div>
                <a href = "{{route('user.removemanager',$user->id)}}">remove manager</a>
                </div>
                @else
                <a href = "{{route('user.makemanager',$user->id)}}">make manager</a>
                @endif
            </td>
        </td>
    
    @endforeach
</table>
@endsection

