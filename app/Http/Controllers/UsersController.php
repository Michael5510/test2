<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Department;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Session;

class UsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {        
        $users = User::all();
        $departments = Department::all();        
        return view('users.index', compact('departments','users'));
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = new User();
        $us = $user->create($request->all());
        $us->save();
        return redirect('users');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = User::findOrFail($id);
        return view('users.show', compact('user'));
    }


    public function changedepartment($uid, $did){
        $user = User::findOrFail($uid);
        if(Gate::allows('change-department', $user)){
            $user->department_id = $did;
            $user->save(); 
            return redirect('users');
        } else {
            Session::flash('notallowed', 'This user has candidates');
        }
        return back();
    }

    public function makemanager($uid, $rid = null){
        Gate::authorize('assign-user');
        $user = User::findOrFail($uid);
        $user->assignRole('manager');
        $user->save(); 
        return back();
    }


    public function removemanager($uid, $rid = null){
        Gate::authorize('assign-user');
        $user = User::findOrFail($uid);
        $user->role_id = $rid;
        $user->save(); 
        return back();
    }



    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::findOrFail($id);
        return view('users.edit', compact('user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = User::findOrFail($id);
        $user->update($request->all());
       return redirect('users');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if(auth()->user()->isAdmin())
        {
            $user = User::findOrFail($id);
            $user->delete(); 
            return redirect('users'); 
        }
        else{
            Session::flash('notallowed', 'You are not allowed to delete users');
        }
        return back();
    }
}
